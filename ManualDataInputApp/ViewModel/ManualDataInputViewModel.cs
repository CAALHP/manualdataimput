﻿using System;
using System.ComponentModel;
using System.Windows;
using System.Windows.Input;
using GalaSoft.MvvmLight;
using GalaSoft.MvvmLight.Command;
using GalaSoft.MvvmLight.Messaging;
using N4CLibrary;
using Plugins.ManualDataInputApp.Model;

namespace Plugins.ManualDataInputApp.ViewModel
{
    /// <summary>
    /// This class contains properties that a View can data bind to.
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class ManualDataInputViewModel : ViewModelBase, IDataErrorInfo
    {
        private readonly IDataService _dataService;
        private double _systolic;
        private double _diastolic;
        private double _pulse;
        private bool _noiseDetected;
        private double _timeSeated;
        private string _error = string.Empty;

        public double Systolic
        {
            get { return _systolic; }
            set { _systolic = value; RaisePropertyChanged("Systolic");}
        }

        public double Diastolic
        {
            get { return _diastolic; }
            set { _diastolic = value; RaisePropertyChanged("Diastolic");}
        }

        public double Pulse
        {
            get { return _pulse; }
            set { _pulse = value; RaisePropertyChanged("Pulse");}
        }

        public bool NoiseDetected
        {
            get { return _noiseDetected; }
            set { _noiseDetected = value; RaisePropertyChanged("NoiseDetected");}
        }

        public double TimeSeated
        {
            get { return _timeSeated; }
            set { _timeSeated = value; RaisePropertyChanged("TimeSeated");}
        }

        private bool IsValidSys
        {
            get
            {
                return (Systolic >= 80 && Systolic <= 300);
            }
        }

        private bool IsValidDia
        {
            get
            {
                return (Diastolic >= 60 && Diastolic <= 240);
            }
        }

        private bool IsValidPulse
        {
            get
            {
                return (Pulse >= 25 && Pulse <= 230);
            }
        }

        private bool IsValidTimeSeated
        {
            get
            {
                return (TimeSeated >= 0 && TimeSeated <= 3600);
            }
        }

        public bool IsValidSubmitData
        {
            get
            {
                return IsValidSys && IsValidDia && IsValidPulse && IsValidTimeSeated;
            }
        }

        /// <summary>
        /// Initializes a new instance of the ManualDataInputViewModel class.
        /// </summary>
        public ManualDataInputViewModel(IDataService dataService)
        {
            _dataService = dataService;
            SubmitDataCommand = new RelayCommand(SubmitDataCommand_Execute);
        }

        #region Command
        public ICommand SubmitDataCommand { get; set; }

        private void SubmitDataCommand_Execute()
        {
            var homeBloodPressure = new HomeBloodPressureObservation(Systolic, Diastolic, Pulse, NoiseDetected, TimeSeated);
            try
            {
                _dataService.UploadHomeBloodPressure(homeBloodPressure);
                MessageBox.Show("Submitted!", "Saved", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            catch (Exception e)
            {
                MessageBox.Show(e.InnerException.Message, "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            
        }
        #endregion

        public string this[string columnName]
        {
            get
            {
                _error = string.Empty;
                if (columnName == "Systolic" && !IsValidSys)
                {
                    _error = "Systolic should be between 80 and 300!";
                }
                else if (columnName == "Diastolic" && !IsValidDia)
                {
                    _error = "Diastolic should be between 60 and 240!";
                }
                else if (columnName == "Pulse" && !IsValidPulse)
                {
                    _error = "Pulse should be between 25 and 230!";
                }
                else if (columnName == "TimeSeated" && !IsValidTimeSeated)
                {
                    _error = "TimeSeated should be between 0 and 3600!";
                }
                
                RaisePropertyChanged("IsValidSubmitData");
                return _error;
            }
        }

        public string Error
        {
            get { return _error; }
        }

        public void Show()
        {
            Messenger.Default.Send(new NotificationMessage("Show"));
        }
    }
}