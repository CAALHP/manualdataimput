﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Windows;
using System.Windows.Threading;
using CAALHP.Contracts;
using CAALHP.Events;
using CAALHP.Utils.Helpers;
using CAALHP.Utils.Helpers.Serialization;
using CAALHP.Utils.Helpers.Serialization.JSON;
using GalaSoft.MvvmLight.Threading;
using IKriv.Wpf;
using Plugins.ManualDataInputApp.ViewModel;
using Plugins.ManualDataInputApp.Views;

namespace Plugins.ManualDataInputApp
{
    public class ManualDataInputAppImplementation : IAppCAALHPContract
    {
        private IAppHostCAALHPContract _host;
        private int _processId;
        private Thread _thread;
        private const string AppName = "ManualDataInputApp";
        //Tell compiler not to optimize the _app variable by declaring it as volatile!
        private volatile Application _app;

        public ManualDataInputAppImplementation()
        {
            InitThread();
            //wait for the _app to be initialized
            //We declare _app as volatile so it won't be cached by optimizations
            //If _app is not volatile the following line could be cached in release mode and never finish.
            while (_app == null) { }
            //_vml = new ViewModelLocator();
            _app.Resources.Add("Locator", new ViewModelLocator());

            //Add datatemplates for subviews
            var manager = new DataTemplateManager(_app);
            manager.RegisterDataTemplate<ManualDataInputViewModel, ManualDataInputView>();

            DispatchToApp(() => new ManualDataInputView().Hide());
        }

        public void Notify(KeyValuePair<string, string> notification)
        {
            var type = EventHelper.GetTypeFromFullyQualifiedNameSpace(notification.Key);
            dynamic obj = JsonSerializer.DeserializeEvent(notification.Value, type);
            HandleEvent(obj);
        }

        private void HandleEvent(ShowAppEvent e)
        {
            if (e.AppName.Equals(AppName))
            {
                Show();
            }
        }

        public string GetName()
        {
            return AppName;
        }

        public bool IsAlive()
        {
            return true;
        }

        public void ShutDown()
        {
            _app.Dispatcher.BeginInvokeShutdown(DispatcherPriority.Normal);
        }

        public void Initialize(IAppHostCAALHPContract hostObj, int processId)
        {
            _host = hostObj;
            _processId = processId;
            _host.Host.SubscribeToEvents(EventHelper.GetFullyQualifiedNameSpace(SerializationType.Json, typeof(ShowAppEvent)), _processId);
        }

        public void Show()
        {
            //ref: http://reedcopsey.com/2011/11/28/launching-a-wpf-window-in-a-separate-thread-part-1/
            //Threading is used so we do not block the main thread.
            DispatchToApp(() =>
            {
                var vml = _app.Resources["Locator"] as ViewModelLocator;
                if (vml != null)
                    vml.ManualDataInput.Show();
            });
        }

        public IList<IPluginInfo> GetListOfInstalledApps()
        {
            throw new NotImplementedException();
        }

        private void InitThread()
        {
            _thread = new Thread(() =>
            {
                _app = new Application();
                _app.ShutdownMode = ShutdownMode.OnExplicitShutdown;
                /*SynchronizationContext.SetSynchronizationContext(
                    new DispatcherSynchronizationContext(
                        Dispatcher.CurrentDispatcher));*/
                DispatcherHelper.Initialize();
                _app.Run();
            });

            //This is needed for GUI threads
            _thread.SetApartmentState(ApartmentState.STA);
            // Make the thread a background thread
            //_thread.IsBackground = true;

            _thread.Start();
        }

        private void DispatchToApp(Action action)
        {
            _app.Dispatcher.Invoke(action);
        }

    }
}