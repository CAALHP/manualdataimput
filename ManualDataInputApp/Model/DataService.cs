﻿using System.Collections.Generic;
using System.Configuration;
using N4CLibrary;

namespace Plugins.ManualDataInputApp.Model
{
    public class DataService : IDataService
    {
        
        public IList<HomeBloodPressureObservation> GetHomeBloodPressures()
        {
            var serverAddress = ConfigurationManager.AppSettings.Get("serveraddress");
            var cpr = ConfigurationManager.AppSettings.Get("cpr");
            var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
            var n4CHelper = new N4CHelper(serverAddress);
            //HomeBloodPressures = new ObservableCollection<HomeBloodPressure>();
            return n4CHelper.GetHomeBloodPressures(cpr, minutes);
        }

        public void UploadHomeBloodPressure(HomeBloodPressureObservation homeBloodPressureObservation)
        {
            var serverAddress = ConfigurationManager.AppSettings.Get("serveraddress");
            var cpr = ConfigurationManager.AppSettings.Get("cpr");
            //var minutes = long.Parse(ConfigurationManager.AppSettings.Get("minutestolookback"));
            var n4CHelper = new N4CHelper(serverAddress);
            n4CHelper.UploadBloodPressureObservation(cpr, homeBloodPressureObservation);
        }
    }
}