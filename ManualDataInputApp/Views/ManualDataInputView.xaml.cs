﻿using GalaSoft.MvvmLight.Messaging;
using GalaSoft.MvvmLight.Threading;

namespace Plugins.ManualDataInputApp.Views
{
    /// <summary>
    /// Description for ManualDataInputView.
    /// </summary>
    public partial class ManualDataInputView
    {
        /// <summary>
        /// Initializes a new instance of the ManualDataInputView class.
        /// </summary>
        public ManualDataInputView()
        {
            InitializeComponent();
            Messenger.Default.Register<NotificationMessage>(this, ShowView);
        }

        private void ShowView(NotificationMessage note)
        {
            if (note.Notification == "Show")
            {
                DispatcherHelper.CheckBeginInvokeOnUI(Show);
                DispatcherHelper.CheckBeginInvokeOnUI(BringToFront);
            }
        }

        private void BringToFront()
        {
            Activate();
        }

        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            e.Cancel = true;
            Hide();
        }

        
    }
}