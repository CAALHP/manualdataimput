﻿using System.AddIn;
using Plugins.AppPluginAdapters;

namespace Plugins.ManualDataInputApp
{
    // The AddInAttribute identifies this pipeline segment as an add-in.
    [AddIn("Manual Data Input App", Version = "1.0.0.0")]
    public class ManualDataInputAppBoilerplate : AppPluginAdapter
    {
        public ManualDataInputAppBoilerplate()
        {
            App = new ManualDataInputAppImplementation();
        }
    }
}
